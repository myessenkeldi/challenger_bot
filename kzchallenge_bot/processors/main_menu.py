from django_tgbot.decorators import processor
from django_tgbot.state_manager import message_types, update_types, state_types
from django_tgbot.types.inlinekeyboardbutton import InlineKeyboardButton
from django_tgbot.types.inlinekeyboardmarkup import InlineKeyboardMarkup
from django_tgbot.types.keyboardbutton import KeyboardButton
from django_tgbot.types.replykeyboardmarkup import ReplyKeyboardMarkup
from django_tgbot.types.update import Update

from kzchallenge_bot.bot import state_manager, TelegramBot
from kzchallenge_bot.models import TelegramState, TelegramUser, Challenge

from .user_profile import statistics, user_settings


def main_menu(bot, chat_id, user, state):
    message = 'Главное меню:'
    state.set_name('main_menu')

    bot.sendMessage(
        chat_id,
        text=message,
        reply_markup=InlineKeyboardMarkup.a(
            inline_keyboard=[
                [
                    InlineKeyboardButton.a('Мои испытания', callback_data='my_challenges'),
                ]
            ]
        )
    )


@processor(state_manager, from_states='main_menu', update_types=[update_types.CallbackQuery])
def send_keyboards(bot: TelegramBot, update: Update, state: TelegramState):
    from .challenges import my_challenges
    chat_id = update.get_chat().get_id()
    user_id = update.get_user().get_id()
    user = TelegramUser.objects.filter(telegram_id=user_id).first()
    callback_data = update.get_callback_query().get_data()
    if callback_data == 'my_challenges':
        my_challenges(bot, chat_id, user, state)
    elif callback_data == 'Статистика':
        statistics(bot, chat_id, user, state)
    elif callback_data == 'Настройки':
        user_settings(bot, chat_id, user, state)
    else:
        state.set_name('')


