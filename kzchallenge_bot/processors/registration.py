from django_tgbot.decorators import processor
from django_tgbot.state_manager import message_types
from django_tgbot.types.keyboardbutton import KeyboardButton
from django_tgbot.types.replykeyboardmarkup import ReplyKeyboardMarkup
from django_tgbot.types.replykeyboardremove import ReplyKeyboardRemove
from django_tgbot.types.update import Update

from .main_menu import main_menu
from kzchallenge_bot.bot import state_manager, TelegramBot
from kzchallenge_bot.models import TelegramState, TelegramUser, GENDER, BUTTONS


def register_user(bot, chat_id, user, state):
    message = 'Здравствуйте!Вас приветсвует бот KZChallenger.' \
              'Для участия в испытаниях пройдите регистрацию.\n' \
              'Выберите свой пол: '. \
        format(first_name=user.first_name, last_name=user.last_name)

    state.set_name('asked_for_gender')

    bot.sendMessage(
        chat_id,
        text=message,
        reply_markup=ReplyKeyboardMarkup.a(
            one_time_keyboard=True,
            resize_keyboard=True,
            keyboard=[
                [
                    KeyboardButton.a(BUTTONS.male),
                    KeyboardButton.a(BUTTONS.female)
                ]
            ]
        )
    )


@processor(state_manager, from_states='asked_for_gender', message_types=[message_types.Text])
def send_keyboards(bot: TelegramBot, update: Update, state: TelegramState):
    chat_id = update.get_chat().get_id()
    user_id = update.get_user().get_id()
    user = TelegramUser.objects.filter(telegram_id=user_id).first()
    text = str(update.get_message().get_text())
    state_passed = True
    state.set_name('')
    if text == 'Мужской':
        user.gender = GENDER.MALE
    elif text == 'Женский':
        user.gender = GENDER.FEMALE
    else:
        state_passed = False

    if state_passed:
        user.save()
        bot.sendMessage(chat_id,
                        'Введите номер телефона',
                        reply_markup=ReplyKeyboardRemove.a(remove_keyboard=True))

        state.set_name('asked_for_phone')


@processor(state_manager, from_states='asked_for_phone', message_types=[message_types.Text])
def send_keyboards(bot: TelegramBot, update: Update, state: TelegramState):
    chat_id = update.get_chat().get_id()
    user_id = update.get_user().get_id()
    user = TelegramUser.objects.filter(telegram_id=user_id).first()
    text = str(update.get_message().get_text())

    user.phone = text
    user.registered = True
    user.save()

    state.set_name('main_menu')
    main_menu(bot, chat_id, user, state)

