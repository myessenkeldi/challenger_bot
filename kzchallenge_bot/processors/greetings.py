from django_tgbot.decorators import processor
from django_tgbot.state_manager import state_types, message_types
from django_tgbot.types.update import Update
from kzchallenge_bot.bot import state_manager, TelegramBot
from kzchallenge_bot.models import TelegramState, TelegramUser

from .main_menu import main_menu
from .registration import register_user


@processor(state_manager)
def say_hello(bot: TelegramBot, update: Update, state: TelegramState):
    chat_id, user_id = None, None
    if update.get_chat():
        chat_id = update.get_chat().get_id()
    if update.get_user():
        user_id = update.get_user().get_id()
    if chat_id and user_id:
        user = TelegramUser.objects.filter(telegram_id=user_id).first()
        if user.registered:
            main_menu(bot, chat_id, user, state)
        else:
            register_user(bot, chat_id, user, state)






