import json

from django_tgbot.decorators import processor
from django_tgbot.state_manager import message_types, state_types, update_types
from django_tgbot.types.inlinekeyboardbutton import InlineKeyboardButton
from django_tgbot.types.inlinekeyboardmarkup import InlineKeyboardMarkup
from django_tgbot.types.replykeyboardmarkup import ReplyKeyboardMarkup
from django_tgbot.types.update import Update
from kzchallenge_bot.bot import state_manager, TelegramBot
from kzchallenge_bot.models import TelegramState, TelegramUser, \
    Challenge, ChallengeResult, ChallengeType, ModeType


from .main_menu import main_menu
from ..utils import check_int, create_string_with_commas


def my_challenges(bot, chat_id, user, state):
    challenges = Challenge.objects.filter(user_id=user.id, is_active=True)
    buttons = []
    for challenge in challenges:
        buttons.append(InlineKeyboardButton.a(
            create_string_with_commas(challenge.types, type='list'),
            callback_data=str(challenge.id)))

    buttons.append(InlineKeyboardButton.a('Добавить испытание', callback_data='add_challenge'))
    buttons.append(InlineKeyboardButton.a('Главное меню', callback_data='main_menu'))

    state.set_name('select_challenge')

    bot.sendMessage(
        chat_id,
        text='Выберите испытание',
        reply_markup=InlineKeyboardMarkup.a(
            inline_keyboard=[
                buttons
            ]
        )
    )


@processor(state_manager, from_states='select_challenge', update_types=[update_types.CallbackQuery])
def select_challenge(bot: TelegramBot, update: Update, state: TelegramState):
    callback_data = update.get_callback_query().get_data()
    chat_id = update.get_chat().get_id()
    user_id = update.get_user().get_id()
    user = TelegramUser.objects.filter(telegram_id=user_id).first()
    if callback_data == 'add_challenge':
        add_challenge(bot, chat_id, user, state)
    elif callback_data == 'main_menu':
        main_menu(bot, chat_id, user, state)
    else:
        try:
            challenge = Challenge.objects.filter(id=int(callback_data), user_id=user.id)
            if challenge:
                _show_challenge(bot, chat_id, user, state, challenge.first())
            else:
                my_challenges(bot, chat_id, user, state)
        except Exception as e:
            my_challenges(bot, chat_id, user, state)


def add_challenge(bot, chat_id, user, state):

    challenge_types = ChallengeType.objects.all()
    buttons = []
    count_in_row = 0
    raw_buttons = []

    for type in challenge_types:
        raw_buttons.append(InlineKeyboardButton.a(type.name, callback_data=str(type.id)))
        count_in_row+=1
        if count_in_row == 2:
            buttons.append(raw_buttons)
            raw_buttons = []
            count_in_row = 0

    raw_buttons.append(InlineKeyboardButton.a('Главное меню', callback_data='main_menu'))
    buttons.append(raw_buttons)

    state.set_name('select_challenge_type')

    bot.sendMessage(
        chat_id,
        text='Выберите один или несколько типов испытания испытания и нажмите сохранить',
        reply_markup=InlineKeyboardMarkup.a(
            inline_keyboard=buttons
        )
    )


@processor(state_manager, from_states='select_challenge_type', update_types=[update_types.CallbackQuery])
def select_challenge_type(bot: TelegramBot, update: Update, state: TelegramState):
    callback_data = update.get_callback_query().get_data()
    chat_id = update.get_chat().get_id()
    user_id = update.get_user().get_id()
    user = TelegramUser.objects.filter(telegram_id=user_id).first()

    if callback_data =='save':
        pressed_buttons = json.loads(state.get_memory().get('pressed_buttons', "[]"))
        pressed_types = ChallengeType.objects.filter(id__in=pressed_buttons)
        state.set_name('add_challenge_data')
        unit = pressed_types.first().group.unit
        if unit == 'км':
            x,y,z = '30','50','1000'
        else:
            x,y,z = '30','500','30'


        bot.sendMessage(
        chat_id,
        text='Введите данные о испытании в следующем формате '
             'количество дней/{unit}/штраф в тенге за 1 {unit}\n'
             'Например: \n{x} {y} {z}'.format(unit=unit,x=x,y=y,z=z),
        )
    elif callback_data =='main_menu':
        state.set_memory({})
        main_menu(bot, chat_id, user, state)
    else:
        pressed_buttons = json.loads(state.get_memory().get('pressed_buttons', "[]"))
        if callback_data == 'delete':
            pressed_buttons = pressed_buttons[:-1]
        else:
            try:
                callback_data = int(callback_data)
                pressed_buttons.append(callback_data)
            except:
                main_menu(bot, chat_id, user, state)
        state.set_memory({
            'pressed_buttons': json.dumps(pressed_buttons)
        })
        pressed_types = ChallengeType.objects.filter(id__in=pressed_buttons)

        if pressed_types:
            show_challenge_types = ChallengeType.objects.filter(group=pressed_types.first().group)
            show_challenge_types = show_challenge_types.exclude(id__in=pressed_types.values_list('id', flat=True))
        else:
             show_challenge_types = ChallengeType.objects.all()

        buttons = []
        raw_buttons = []
        count_in_row = 0

        for type in show_challenge_types:
            raw_buttons.append(InlineKeyboardButton.a(type.name, callback_data=str(type.id)))
            count_in_row+=1
            if count_in_row == 2:
                buttons.append(raw_buttons)
                raw_buttons = []
                count_in_row = 0
        if raw_buttons:
            buttons.append(raw_buttons)

        if pressed_types:
            buttons.append([InlineKeyboardButton.a('Удалить', callback_data='delete'),
                            InlineKeyboardButton.a('Сохранить', callback_data='save')])
        buttons.append([InlineKeyboardButton.a('Главное меню', callback_data='main_menu')])

        state.set_name('select_challenge_type')

        if pressed_types:
            text = "Выбраны следующие испытания: "
            text = text + create_string_with_commas(pressed_types, 'qs')
        else:
            text = "Выберите как минимум одно испытание."


        bot.sendMessage(
            chat_id,
            text=text,
            reply_markup=InlineKeyboardMarkup.a(
                buttons
            )
        )


@processor(state_manager, from_states='add_challenge_data', message_types=[message_types.Text])
def add_challenge_data(bot: TelegramBot, update: Update, state: TelegramState):
    from ..api.bot import BotApi
    chat_id = update.get_chat().get_id()
    user_id = update.get_user().get_id()
    type_ids = json.loads(state.get_memory().get('pressed_buttons', "[]"))
    types = ChallengeType.objects.filter(id__in=type_ids)
    data = str(update.get_message().get_text()).split('-')
    days = int(data[0])
    goal = int(data[1])
    penalty = int(data[2])
    mode = ModeType.objects.filter(code='single')
    BotApi.add_challenge(bot, chat_id, state, days, goal, penalty,  user_id, types, mode)


def _show_challenge(bot, chat_id, user, state, challenge):
    from ..api.bot import BotApi

    state.set_name('challenge_selected')

    challenge_info = BotApi.get_challenge_info(challenge)

    text = 'Испытание {challenge_name} на {total_required} {unit}.' \
           'Выполнено {total_done} {unit} на {done_percentage}%'.format(**challenge_info)

    bot.sendMessage(
        chat_id,
        text=text,
        reply_markup=InlineKeyboardMarkup.a(
            inline_keyboard=[
                [
                    InlineKeyboardButton.a(
                        'Добавить результат',
                        callback_data='add_result={challenge_id}'.format(challenge_id=challenge.id)),
                    InlineKeyboardButton.a(
                        'Удалить испытание',
                        callback_data='remove_challenge={challenge_id}'.format(challenge_id=challenge.id)),
                    InlineKeyboardButton.a(
                        'Назад',
                        callback_data='back'),
                    InlineKeyboardButton.a(
                        'Главное Меню',
                        callback_data='main_menu')
                ]
            ]
        )
    )


@processor(state_manager, from_states='challenge_selected', update_types=[update_types.CallbackQuery])
def show_challenge(bot: TelegramBot, update: Update, state: TelegramState):
    callback_data = update.get_callback_query().get_data()
    chat_id = update.get_chat().get_id()
    user_id = update.get_user().get_id()
    user = TelegramUser.objects.filter(telegram_id=user_id).first()
    if 'add_result' in callback_data:
        add_challenge_result(bot, chat_id, user, state, callback_data)
    elif 'remove_challenge' in callback_data:
        remove_challenge(bot, chat_id, user, state, callback_data)
    elif 'main_menu' in callback_data:
        main_menu(bot, chat_id, user, state)
    elif callback_data == 'back':
        my_challenges(bot, chat_id, user, state)
    else:
        # challenge = Challenge.objects.get(id=callback_data)
        # _show_challenge(bot, chat_id, user, state, challenge)
        pass


def get_callback_data_by_param(param, callback_data):
    return callback_data[len(param)+1:]


def add_challenge_result(bot, chat_id, user, state, callback_data):
    id = get_callback_data_by_param('add_result', callback_data)
    state.set_name({
        'id':id
    })
    state.set_name('add_challenge_result')
    bot.sendMessage(
        chat_id,
        text='Введите результат за сегодняшний день в формате'
             'Тип испытания/количество выполненное за день. \nНапример:\n'
             'Бег 2\n'
             'Чтение 400',
    )


def remove_challenge(bot, chat_id, user, state, callback_data):
    id = get_callback_data_by_param('remove_challenge', callback_data)
    challenge = Challenge.objects.filter(id=id)
    challenge.delete()
    bot.sendMessage(
        chat_id,
        text='Испытание успешно удалено',
    )
    my_challenges(bot, chat_id, user, state)


@processor(state_manager, from_states='add_challenge_result', message_types=[message_types.Text])
def _add_challenge_result(bot: TelegramBot, update: Update, state: TelegramState):
    from ..api.bot import BotApi
    chat_id = update.get_chat().get_id()
    user_id = update.get_user().get_id()
    user = TelegramUser.objects.filter(telegram_id=user_id).first()
    data = str(update.get_message().get_text()).split(' ')
    quantity = check_int(data[1], bot, chat_id)
    challenge_type_name = data[0].lower()
    challenge_id = state.get_memory().get('id', None)
    challenge = Challenge.objects.get(id=challenge_id)
    challenge_type = ChallengeType.objects.filter(name__icontains=challenge_type_name)
    state = BotApi.add_challenge_result(bot,chat_id, challenge_type, quantity)
    if state:
        _show_challenge(bot,chat_id,user,state,challenge)
