from django.db.models import Q
from django_tgbot.decorators import processor
from django_tgbot.state_manager import message_types, update_types, state_types
from django_tgbot.types.keyboardbutton import KeyboardButton
from django_tgbot.types.replykeyboardmarkup import ReplyKeyboardMarkup
from django_tgbot.types.update import Update

from kzchallenge_bot.bot import state_manager, TelegramBot
from kzchallenge_bot.models import TelegramState, TelegramUser, Challenge, \
    ChallengeType, ModeType
from kzchallenge_bot.apps import KzChallengeBotConfig
from kzchallenge_bot.processors.main_menu import main_menu
from kzchallenge_bot.decorators import command_processor
from kzchallenge_bot.api.bot import BotApi
from kzchallenge_bot.utils import check_int, generate_examples


@processor(state_manager, from_states=state_types.All, message_types=[message_types.Text], update_types=update_types.Command)
def send_keyboards(bot: TelegramBot, update: Update, state: TelegramState):
    chat_id = update.get_chat().get_id()
    user_id = update.get_user().get_id()
    user = TelegramUser.objects.filter(telegram_id=user_id).first()
    data = str(update.get_message().get_text())[1:].split(' ')
    command = data[0]
    data = data[1:]
    handle(command, bot, chat_id, user, state, update, data)


def handle(command, bot, chat_id, user, state, update, data):
    for proc in KzChallengeBotConfig.command_processors:
        if command in proc['commands']:
            proc['func'](bot, chat_id, user, state, update, data)


@command_processor(commands = ['start'])
def start(bot, chat_id, user, state, update, data):
        main_menu(bot, chat_id, user, state)


@command_processor(commands = ['create'])
def create(bot, chat_id, user, state, update, data):
    user_id = update.get_user().get_id()

    type_names = data[0].split(',')
    days = check_int(data[1], bot, chat_id)
    goal = check_int(data[2], bot, chat_id)
    penalty = check_int(data[3], bot, chat_id)

    if type_names and days and goal and penalty:
        types = []
        error_types = []
        for type_name in type_names:
            type = ChallengeType.objects.filter(Q(name__icontains=type_name) | Q(name__in=type_name))
            if type:
                types.append(type.first())
            else:
                error_types.append(type_name)
        if error_types:
            bot.sendMessage(
                chat_id,
                text='Ошибка в следующих типах испытаний: {error_types}'.format(error_types=error_types),
            )
        else:
            mode = ModeType.objects.filter(code='group')
            BotApi.add_challenge(bot, chat_id, state, days, goal, penalty,  user_id, types, mode)

            for types in range(len(type_names)):
                '/add  '.format(error_types=error_types),


            bot.sendMessage(
                chat_id,
                text='Для внесения результатов в испытание введите'
                     'команду /add в формате:\n /add Тип испытания/'
                     'количество выполненного за сегодня\n'
                     'Например:\n' + generate_examples(type_names,
                                                       mask='/add {x} 20',
                                                       type='list')

            )


@command_processor(commands = ['add'])
def add(bot, chat_id, user, state, update, data):
    chat_id = update.get_chat().get_id()
    user_id = update.get_user().get_id()
    user = TelegramUser.objects.filter(telegram_id=user_id).first()
    data = str(update.get_message().get_text()).split(' ')
    quantity = check_int(data[1], bot, chat_id)
    challenge_type_name = data[0].lower()
    challenge_id = state.get_memory().get('id', None)
    challenge = Challenge.objects.get(id=challenge_id)
    challenge_type = ChallengeType.objects.filter(name__icontains=challenge_type_name)
    state = BotApi.add_challenge_result(bot,chat_id,challenge_type,quantity)
    if state:
        challenge_info = BotApi.get_challenge_info(challenge)
        text = 'Испытание {challenge_name} на {total_required} {unit}.' \
               'Выполнено {total_done} {unit} на {done_percentage}%'.format(**challenge_info)
        bot.sendMessage(chat_id, text=text)