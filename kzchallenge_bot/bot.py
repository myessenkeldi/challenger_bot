from django_tgbot.bot import AbstractTelegramBot
from django_tgbot.models import AbstractTelegramState
from django_tgbot.state_manager import update_types
from django_tgbot.state_manager.state_manager import StateManager
from django_tgbot.types.update import Update
from . import bot_token
from .models import TelegramUser, TelegramChat, TelegramState


class TelegramBot(AbstractTelegramBot):
    def __init__(self, token, state_manager):
        super(TelegramBot, self).__init__(token, state_manager)

    def get_db_user(self, telegram_id):
        return TelegramUser.objects.get_or_create(telegram_id=telegram_id)[0]

    def get_db_chat(self, telegram_id):
        return TelegramChat.objects.get_or_create(telegram_id=telegram_id)[0]

    def get_db_state(self, db_user, db_chat):
        return TelegramState.objects.get_or_create(telegram_user=db_user, telegram_chat=db_chat)[0]

    def pre_processing(self, update: Update, user, db_user, chat, db_chat, state):
        super(TelegramBot, self).pre_processing(update, user, db_user, chat, db_chat, state)

    def post_processing(self, update: Update, user, db_user, chat, db_chat, state):
        super(TelegramBot, self).post_processing(update, user, db_user, chat, db_chat, state)


def import_processors():
    from kzchallenge_bot import processors


update_types.Command = 'command'


class ChallengerStateManager(StateManager):
    def get_processors(self, update: Update, state: AbstractTelegramState):
        message = update.get_message()
        message_type = message.type() if message is not None else None
        update_type = update.type()
        state_name = state.name
        if message and hasattr(message, 'entities'):
            for entity in message.entities:
                if hasattr(entity, 'type') and entity.type=='bot_command':
                    update_type = 'command'
        processors = []
        for handling_state in self.handling_states.keys():
            if handling_state.matches(state_name=state_name, update_type=update_type, message_type=message_type):
                processors.append(self.handling_states[handling_state])
        return processors


state_manager = ChallengerStateManager()
bot = TelegramBot(bot_token, state_manager)
import_processors()
