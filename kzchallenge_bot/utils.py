
def check_int(num, bot, chat_id):
    try:
        return int(num)
    except:
        bot.sendMessage(
            chat_id,
            text='Ошибка в значении: {num}'.format(num=num),
        )
        return None


def create_string_with_commas(objs, type = 'qs'):

    if type=='qs':
        text = objs.first().name
        for pr_type in objs.exclude(id=objs.first().id):
            text = text + ', '+ pr_type.name
    elif type=='list':
        text = objs.first().name
        for pr_type in objs.exclude(id=objs.first().id):
            text = text + ', '+ pr_type.name
    else:
        text=None
    return text


def generate_examples(objs, mask='{x}', type='list'):
    res = ''
    for i in range(len(objs)):
        res = res + mask.format(x=create_string_with_commas(objs[i:], type=type)) + '\n'

    return res


