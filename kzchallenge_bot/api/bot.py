from datetime import datetime, timedelta

from django.db.models import ExpressionWrapper, F, fields
from kzchallenge_bot.models import ChallengeResult

from kzchallenge_bot.processors.challenges import _show_challenge

from kzchallenge_bot.models import TelegramUser, Challenge

from kzchallenge_bot.utils import create_string_with_commas


class BotApi(object):

    @staticmethod
    def add_challenge_result(bot, chat_id, challenge_type, quantity):
        if challenge_type:
            challenge_result = ChallengeResult(challenge_type=challenge_type.first(), quantity=quantity)
            challenge_result.save()
            return True
        else:
            bot.sendMessage(
                chat_id,
                text='Введены неверные данные, повторите ввод',
            )
            return False

    @staticmethod
    def add_challenge(bot, chat_id, state, days, goal, penalty, user_id, types, mode):
        try:
            user = TelegramUser.objects.filter(telegram_id=user_id).first()
            challenge = Challenge(
                user=user,
                end_date=datetime.now() + timedelta(days=days),
                goal=goal,
                penalty=penalty,
                mode=mode
            )
            challenge.save()
            for ch_type in types:
                challenge.types.add(ch_type)
            bot.sendMessage(
                chat_id,
                text='Испытание успешно создано и активно с сегодняшнего дня!'
                     'Пожалуйста внестите результат до 00:00.',
            )
            _show_challenge(bot, chat_id, user, state, challenge)
        except Exception as e:
            bot.sendMessage(
                chat_id,
                text='Что то пошло не так..., повторите ввод.',
            )

    @staticmethod
    def get_challenge_info(challenge):
        results = challenge.results.all()
        total_result = 0
        for result in results:
            q = result.quantity
            mask = result.type.result_formula.format(x=str(q))
            res = eval(mask)
            total_result += res

        unit = challenge.types.first().group.unit

        return {'challenge_name': create_string_with_commas(challenge.types),
                'total_required': str(challenge.goal),
                'unit': unit,
                'total_done': str(total_result),
                'done_percentage': str(total_result or 1/ int(challenge.goal) * 100),
                'penalty': str(challenge.goal-total_result*challenge.penalty)
                }
