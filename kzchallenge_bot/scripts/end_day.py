from datetime import timedelta

from django.utils import timezone

from kzchallenge_bot.models import Challenge, TelegramChat

from kzchallenge_bot.api.bot import BotApi

from kzchallenge_bot.bot import bot


def run(*args, **kwargs):
    challenges = Challenge.objects.filter(end_date__lt=timezone.now())
    challenges.update(is_active=False)

    chats = TelegramChat.objects.all()

    for chat in chats:
        count = 1
        text = 'Результаты дня:\n'
        chat_challenges = challenges.filter(chat=chat)
        if chat_challenges:
            for index in range(chat_challenges.count()):
                chat_challenge = chat_challenges[index]
                challenge_info = BotApi.get_challenge_info(chat_challenge)
                if int(challenge_info['done_percentage']) >=100:
                    text = text + '{count}){user} прошел испытание \n' \
                                  '{challenge_name}({total_done}/{goal}) \n' \
                                  'на {done_percentage}% \n' \
                                  .format(**challenge_info,
                                          count=str(count),
                                          user=chat_challenge.user
                                          )
                else:
                    text = text + '{count}){user} не прошел испытание \n' \
                                  '{challenge_name}({total_done}/{goal}) \n' \
                                  'на {done_percentage}% \n' \
                                  'и получает штраф - {penalty}.' \
                        .format(**challenge_info,
                                count=str(count),
                                user=chat_challenge.user,
                                )
        bot.sendMessage(chat_id=chat.id, text=text)