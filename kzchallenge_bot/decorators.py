from kzchallenge_bot.apps import KzChallengeBotConfig


def command_processor(commands=None):
    def wrapper(func):
        KzChallengeBotConfig.command_processors.append({'commands':commands,
                                                        'func': func})
    return wrapper