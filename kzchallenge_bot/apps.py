from django.apps import AppConfig


class KzChallengeBotConfig(AppConfig):
    command_processors = []

    def append_command_processor(self, processor):
        self.command_processors.append(processor)

    def ready(self):
        import kzchallenger_bot.managers
