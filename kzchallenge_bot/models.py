from django.db import models
from django.db.models import CASCADE, ManyToManyField

from django_tgbot.models import AbstractTelegramUser, AbstractTelegramChat, AbstractTelegramState


class GENDER(object):
    MALE = 'Мужской'
    FEMALE = 'Женский'


class BUTTONS(object):
    male = 'Мужской'
    female = 'Женский'


class TelegramUser(AbstractTelegramUser):
    GENDER_TYPE = [
        ('Мужской', 'Мужской'),
        ('Женский', 'Женский'),
    ]

    gender = models.CharField(max_length=25, choices=GENDER_TYPE, default=GENDER.MALE, verbose_name=('пол'), )
    phone_number = models.CharField(max_length=25)
    registered = models.BooleanField(default=False)

    def __str__(self):
        if self.first_name and self.last_name:
            return "{} {}".format(self.first_name, self.last_name)
        elif self.username:
            return "@{}".format(self.username)
        elif self.first_name:
            return "{}".format(self.first_name)
        elif self.last_name:
            return "{}".format(self.last_name)
        else:
            return '@user'


class TelegramChat(AbstractTelegramChat):
    pass


class TelegramState(AbstractTelegramState):
    telegram_user = models.ForeignKey(TelegramUser, related_name='telegram_states', on_delete=CASCADE, blank=True, null=True)
    telegram_chat = models.ForeignKey(TelegramChat, related_name='telegram_states', on_delete=CASCADE, blank=True, null=True)

    class Meta:
        unique_together = ('telegram_user', 'telegram_chat')


class ChallengeTypeGroup(models.Model):
    name = models.CharField(max_length=50)
    unit = models.CharField(max_length=50, default='км')


class ChallengeType(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)
    result_formula = models.CharField(max_length=255)
    group = models.ForeignKey(ChallengeTypeGroup, on_delete=models.CASCADE, default=1)


class ModeType(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=50)


class Challenge(models.Model):

    chat = models.ForeignKey(TelegramChat, on_delete=models.CASCADE, default=1)
    user = models.ForeignKey(TelegramUser, on_delete=models.CASCADE)
    start_date = models.DateField(auto_now_add=True)
    end_date = models.DateField(auto_now_add=True)
    goal = models.IntegerField(blank=True, null=True)
    penalty = models.IntegerField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    done_persentage = models.CharField(max_length=50)
    done_count = models.CharField(max_length=50)
    mode = models.ForeignKey(ModeType, on_delete=models.CASCADE, default=1)


class ChallengeResult(models.Model):
    enter_date = models.DateField(auto_now_add=True)
    quantity = models.IntegerField(blank=True, null=True)
    challenge = models.ForeignKey(Challenge, on_delete=models.CASCADE, related_name='results')
    type = models.ForeignKey(ChallengeType, on_delete=models.CASCADE)


class Rank(models.Model):
    name = models.CharField(max_length=255)
    access_level = models.CharField(max_length=50)

    def __str__(self):
        return '{level} уровень - {name}'.format(level=self.access_level, name=self.name)


class UserRank(models.Model):
    user = models.ForeignKey(TelegramUser, on_delete=models.CASCADE)
    challenges_count = models.CharField(max_length=50)
    access_rating = models.CharField(max_length=50)
    level = models.CharField(max_length=50)
    rank = models.ForeignKey(Rank, on_delete=models.CASCADE, default=1)
    mode = models.ForeignKey(ModeType, on_delete=models.CASCADE, default=1)
