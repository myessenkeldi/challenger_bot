from django.contrib import admin
from django.contrib.admin import ModelAdmin

from kzchallenge_bot.models import TelegramUser, ChallengeType, Challenge, \
    ChallengeResult, ChallengeTypeGroup, ModeType, Rank, UserRank


class TelegramUserAdmin(ModelAdmin):
    list_display = ['first_name', 'last_name', 'username','gender', 'phone_number', 'registered']


class ChallengeTypeGroupAdmin(ModelAdmin):
    list_display = ['name']


class ChallengeTypeAdmin(ModelAdmin):
    list_display = ['name', 'code', 'result_formula', 'get_group']
    raw_id_fields = ['group', ]

    def get_group(self, obj):
        return obj.group.name


class ChallengeAdmin(ModelAdmin):
    list_display = ['get_types', 'user', 'chat', 'is_active','goal', 'mode']

    def get_types(self, obj):
        text = ''
        text = text + obj.first().name
        for pr_type in obj.exclude(id=obj.first().id):
            text = text + ', '+ pr_type.name

        return text


class ChallengeResultAdmin(ModelAdmin):
    list_display = ['enter_date', 'quantity', 'challenge']


class ModeTypeAdmin(ModelAdmin):
    list_display = ['name', 'code']


class RankAdmin(ModelAdmin):
    list_display = ['name', 'access_level']


class UserRankAdmin(ModelAdmin):
    list_display = ['user', 'challenges_count', 'access_rating', 'rank', 'mode']


admin.site.register(TelegramUser, TelegramUserAdmin)
admin.site.register(ChallengeType, ChallengeTypeAdmin)
admin.site.register(Challenge, ChallengeAdmin)
admin.site.register(ChallengeResult, ChallengeResultAdmin)
admin.site.register(ChallengeTypeGroup, ChallengeTypeGroupAdmin)
admin.site.register(ModeType, ModeTypeAdmin)
admin.site.register(Rank, RankAdmin)
admin.site.register(UserRank, UserRankAdmin)

